<?php

namespace App\Containers\Authorization\Tasks;

use App\Containers\Authorization\Data\Repositories\PermissionRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Containers\Authorization\Models\Permission;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeletePermissionTask extends Task
{

    protected $repository;

    public function __construct(PermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($permission) : bool
    {

	if ($permission instanceof Permission) {
            $permission = $permission->id;
        }


        try {
            return $this->repository->delete($permission);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
