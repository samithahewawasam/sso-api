<?php

/**
 * @apiGroup           Authorization
 * @apiName            CreatePermission
 *
 * @api                {POST} /v1/permissions Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('permissions', [
    'as' => 'api_authorization_create_permission',
    'uses'  => 'Controller@CreatePermission',
    'middleware' => [
      'auth:api',
    ],
]);
