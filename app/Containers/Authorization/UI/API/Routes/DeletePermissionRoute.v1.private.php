<?php

/**
 * @apiGroup           Authorization
 * @apiName            deletePermission
 *
 * @api                {DELETE} /v1/permissions/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('permissions/{id}', [
    'as' => 'api_authorization_delete_permission',
    'uses'  => 'Controller@deletePermission',
    'middleware' => [
      'auth:api',
    ],
]);
