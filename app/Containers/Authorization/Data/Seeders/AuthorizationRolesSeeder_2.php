<?php

namespace App\Containers\Authorization\Data\Seeders;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Seeders\Seeder;

/**
 * Class AuthorizationRolesSeeder_2
 *
 * @author  Mahmoud Zalt  <mahmoud@zalt.me>
 */
class AuthorizationRolesSeeder_2 extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default Roles ----------------------------------------------------------------
        Apiato::call('Authorization@CreateRoleTask', ['admin', 'Administrator', 'Administrator Role', 999]);
	Apiato::call('Authorization@CreateRoleTask', ['project_manager', 'Project Manager', 'Access for all KP groups, Approval for data publish, Published data edit option', 900]);
        Apiato::call('Authorization@CreateRoleTask', ['me_management', 'M&E Management', 'Access for all KP groupsApproval for data publishPublished data edit option', 800]);
        Apiato::call('Authorization@CreateRoleTask', ['me_officials', 'M&E Officials', 'Access for assigned KP groupsData adding option (only foridentified key indicators as manualupload)', 700]);
        Apiato::call('Authorization@CreateRoleTask', ['project_officers_coodinators', 'Project Officers/Project Coordinators', 'Access for assigned KP groups', 600]);
        Apiato::call('Authorization@CreateRoleTask', ['lead_cso', 'Lead CSO', 'Access for assigned KP groupswithin assigned districts', 500]);
        Apiato::call('Authorization@CreateRoleTask', ['cso_coodinator', 'CSO coordinator', 'Access for all KP groups withinassigned districts', 400]);
        Apiato::call('Authorization@CreateRoleTask', ['cso_officers', 'CSO Officers','Access for assigned KP groupswithin assigned districts', 400]);
        Apiato::call('Authorization@CreateRoleTask', ['program_manager', 'Program Manager', 'Access for all KP groupsApproval for data publishPublished data edit option', 300]);
        Apiato::call('Authorization@CreateRoleTask', ['coodinator_simulator', 'Coordinator/Simulator', 'Data adding option (only foridentified key indicators as manualupload)', 200]);

        // ...

    }
}
