<?php

namespace App\Containers\Alert\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class AlertRepository
 */
class AlertRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
