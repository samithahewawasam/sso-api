<?php

namespace App\Containers\Alert\Tasks;

use App\Containers\Alert\Data\Repositories\AlertRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateAlertTask extends Task
{

    protected $repository;

    public function __construct(AlertRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
