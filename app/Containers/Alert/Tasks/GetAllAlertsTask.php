<?php

namespace App\Containers\Alert\Tasks;

use App\Containers\Alert\Data\Repositories\AlertRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllAlertsTask extends Task
{

    protected $repository;

    public function __construct(AlertRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
