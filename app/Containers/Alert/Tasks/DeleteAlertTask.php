<?php

namespace App\Containers\Alert\Tasks;

use App\Containers\Alert\Data\Repositories\AlertRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteAlertTask extends Task
{

    protected $repository;

    public function __construct(AlertRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
