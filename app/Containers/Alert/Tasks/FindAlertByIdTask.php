<?php

namespace App\Containers\Alert\Tasks;

use App\Containers\Alert\Data\Repositories\AlertRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindAlertByIdTask extends Task
{

    protected $repository;

    public function __construct(AlertRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
