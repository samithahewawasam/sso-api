<?php

namespace App\Containers\Alert\UI\API\Controllers;

use App\Containers\Alert\UI\API\Requests\CreateAlertRequest;
use App\Containers\Alert\UI\API\Requests\DeleteAlertRequest;
use App\Containers\Alert\UI\API\Requests\GetAllAlertsRequest;
use App\Containers\Alert\UI\API\Requests\FindAlertByIdRequest;
use App\Containers\Alert\UI\API\Requests\UpdateAlertRequest;
use App\Containers\Alert\UI\API\Transformers\AlertTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Alert\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateAlertRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createAlert(CreateAlertRequest $request)
    {
        $alert = Apiato::call('Alert@CreateAlertAction', [$request]);

        return $this->created($this->transform($alert, AlertTransformer::class));
    }

    /**
     * @param FindAlertByIdRequest $request
     * @return array
     */
    public function findAlertById(FindAlertByIdRequest $request)
    {
        $alert = Apiato::call('Alert@FindAlertByIdAction', [$request]);

        return $this->transform($alert, AlertTransformer::class);
    }

    /**
     * @param GetAllAlertsRequest $request
     * @return array
     */
    public function getAllAlerts(GetAllAlertsRequest $request)
    {
        $alerts = Apiato::call('Alert@GetAllAlertsAction', [$request]);

        return $this->transform($alerts, AlertTransformer::class);
    }

    /**
     * @param UpdateAlertRequest $request
     * @return array
     */
    public function updateAlert(UpdateAlertRequest $request)
    {
        $alert = Apiato::call('Alert@UpdateAlertAction', [$request]);

        return $this->transform($alert, AlertTransformer::class);
    }

    /**
     * @param DeleteAlertRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAlert(DeleteAlertRequest $request)
    {
        Apiato::call('Alert@DeleteAlertAction', [$request]);

        return $this->noContent();
    }
}
