<?php

/**
 * @apiGroup           Alert
 * @apiName            updateAlert
 *
 * @api                {PATCH} /v1/alerts/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('alerts/{id}', [
    'as' => 'api_alert_update_alert',
    'uses'  => 'Controller@updateAlert',
    'middleware' => [
      'auth:api',
    ],
]);
