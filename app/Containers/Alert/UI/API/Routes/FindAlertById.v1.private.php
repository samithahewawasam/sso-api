<?php

/**
 * @apiGroup           Alert
 * @apiName            findAlertById
 *
 * @api                {GET} /v1/alerts/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('alerts/{id}', [
    'as' => 'api_alert_find_alert_by_id',
    'uses'  => 'Controller@findAlertById',
    'middleware' => [
      'auth:api',
    ],
]);
