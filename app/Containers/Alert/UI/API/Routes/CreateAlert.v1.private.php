<?php

/**
 * @apiGroup           Alert
 * @apiName            createAlert
 *
 * @api                {POST} /v1/alerts Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('alerts', [
    'as' => 'api_alert_create_alert',
    'uses'  => 'Controller@createAlert',
    'middleware' => [
      'auth:api',
    ],
]);
