<?php

/**
 * @apiGroup           Alert
 * @apiName            deleteAlert
 *
 * @api                {DELETE} /v1/alerts/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('alerts/{id}', [
    'as' => 'api_alert_delete_alert',
    'uses'  => 'Controller@deleteAlert',
    'middleware' => [
      'auth:api',
    ],
]);
