<?php

namespace App\Containers\Alert\UI\API\Transformers;

use App\Containers\Alert\Models\Alert;
use App\Ship\Parents\Transformers\Transformer;

class AlertTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Alert $entity
     *
     * @return array
     */
    public function transform(Alert $entity)
    {
        $response = [
            'object' => 'Alert',
            'id' => $entity->getHashedKey(),
            'description' => $entity->description,
            'seen' => $entity->seen,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
