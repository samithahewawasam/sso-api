<?php

namespace App\Containers\Alert\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteAlertAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Alert@DeleteAlertTask', [$request->id]);
    }
}
