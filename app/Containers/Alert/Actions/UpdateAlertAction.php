<?php

namespace App\Containers\Alert\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateAlertAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $alert = Apiato::call('Alert@UpdateAlertTask', [$request->id, $data]);

        return $alert;
    }
}
