<?php

namespace App\Containers\Alert\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllAlertsAction extends Action
{
    public function run(Request $request)
    {
        $alerts = Apiato::call('Alert@GetAllAlertsTask', [], ['addRequestCriteria']);

        return $alerts;
    }
}
