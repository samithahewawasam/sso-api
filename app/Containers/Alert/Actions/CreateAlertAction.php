<?php

namespace App\Containers\Alert\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateAlertAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $alert = Apiato::call('Alert@CreateAlertTask', [$data]);

        return $alert;
    }
}
