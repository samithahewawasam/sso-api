<?php

namespace App\Containers\Alert\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindAlertByIdAction extends Action
{
    public function run(Request $request)
    {
        $alert = Apiato::call('Alert@FindAlertByIdTask', [$request->id]);

        return $alert;
    }
}
